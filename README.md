# Noëlle Anthony
### 765-889-2772
### noelle.d.anthony@gmail.com
### https://github.com/joyeusenoelle • http://noelleanthony.com

I’ve been working with computers for thirty years and programming for the web for twenty of those. I am well-versed in HTML, including HTML5, and Javascript, including ES6 and jQuery, on the front end, and PHP, Python, MySQL, and Apache on the back end; I have also worked with ColdFusion, ASP, and Ruby, as well as Node.js and Angular 1.0.

I have a bachelor’s degree in Classical Studies from Earlham College, and experience in live theater - on and off stage. Additionally, I have worked with statistics and data management, and am published both scholastically and in fiction. All of this helps me bring a unique perspective to my programming and development.

## Experience
#### Freelance Developer - January 2010-present
* Designed and developed websites for clients, using HTML, PHP, Javascript/jQuery, and MySQL.
* Developed themes and plugins for the WordPress CMS.
* Designed and developed a one-to-many webinar system in Flash.
* Developed an interactive bot in Python for use with a MUSH-based chat room: https://github.com/joyeusenoelle/BotEl/
* Developed a web app for use with the #MyVanityFairCover hashtag: http://www.noelleanthony.com/MyVanityFairCover/
* Consulted with clients on user experience under the brand The Delightworks.
* Selected clients include:
    * Elizabeth McCoy, author: http://www.elizabeth-mccoy.com
    * Genevieve Cogman, author: http://www.grcogman.com
    * The Fluent Self: http://www.thefluentself.com

#### Developer and Customer Relations - IttyBiz - December 2010-January 2012
* Developed WordPress theme and plugins for the IttyBiz website, http://www.ittybiz.com
* Managed the IttyBiz online store and mailing list.
* Provided customer management and custom fulfillment for special orders.

#### Research Assistant - The Johns Hopkins University - September 2002-December 2009
* Primary research assistant to Dr. Gail Daumit at the Welch Center in the Bloomberg School of Public Health.
* Managed and analyzed HIPAA-protected health data.
* Wrote data-related sections of journal articles.
* Developed web-based data-collection instruments using ColdFusion and Microsoft IIS.

#### Developer - Prometric/Thomson Learning - May-August 2000
* Developed the company’s first intranet site, using ASP and MySQL, as the sole developer on the team.

#### Web Developer - The Johns Hopkins University - August 1996-August 1997, May-August 1998
* Developed some of the first websites for the Department of Mental Hygiene in the Bloomberg School of Public Health, under the guidance of the lead developer.

## Education
#### Bachelor of Arts in Ancient and Classical Studies - Earlham College
* Graduated May 2007
* Other significant academic focuses: Theatre, Physics and Astronomy, Japanese Studies

#### Ongoing
* Machine Learning Foundations at Coursera, certificate track: https://www.coursera.org/learn/ml-foundations